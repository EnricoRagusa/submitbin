#!/bin/bash 

NAME=$1
JNAME=$NAME
CORES=$2
TIME=$3
NPAR=2
TEMPLATE=submission_file_template_FargoGPU.pbs
PROJECTID="DIRAC-DP119-GPU"


if [ "$#" -lt $NPAR ]; then

    echo "Missing JobName NGPU and Time arguments! Add them..."

else

sed 's/NAMEFILE/'$NAME'/g' ~/submitbin/$TEMPLATE| sed 's/JOBNAME/'$JNAME'/g'|sed 's/TIME/'$TIME'/g'|sed 's/CORES/'$CORES'/g'|sed 's/PROJECTID/'$PROJECTID'/g' > "sub_"$CORES"GPU_$JNAME.pbs"

fi
